<?php
/**
 * Contrôleur de gestion des représentation
 * @author Lucas
 ** @version 2018
 */

namespace controleur;


use modele\dao\Bdd;
use vue\representation\VueConsultationRepresentation;
use vue\representation\VueSaisieRepresentation;
use vue\representation\VueSupprimerRepresentation;
use modele\dao\RepresentationDAO;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;
use modele\metier\Representation;

class CtrlRepresentation extends ControleurGenerique {
    
    /** controleur= representation & action= defaut
     * Afficher la liste des representation     */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representation & action= consulter
     * Afficher la liste des representation       */
    function consulter() {
        $laVue = new VueConsultationRepresentation();
        $this->vue = $laVue;
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - representation");
        $this->vue->afficher();
    }

/** controleur= representation & action=modifier & id = n° represnetation
     * Afficher le formulaire de modification d'une representation    */
    public function modifier() {
        $idRepres = $_GET["id"];
        $laVue = new VueSaisieRepresentation();
        $this->vue = $laVue;
        // Lire dans la BDD les données de la representation à modifier
        Bdd::connecter();
        $laVue->setUneRepresentation(RepresentationDAO::getOneById($idRepres));
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laVue->getUneRepresentation()->getId());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }

  /** controleur= representation & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
  public function validerModifier() {
        Bdd::connecter();
        $nomLieu = $_REQUEST['lieu'];
        $nomLieu = LieuDAO::getOneByNom($nomLieu);
        $nomGroupe = $_REQUEST['groupe'];
        $nomGroupe = GroupeDAO::getOneByNom($nomGroupe);
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['date'], $nomLieu , $nomGroupe, $_REQUEST['heuredebut'], $_REQUEST['heurefin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRepresentation($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            RepresentationDAO::update($uneRepresentation->getId(), $uneRepresentation);
            // revenir à la liste des Groupes
            header("Location: index.php?controleur=representation&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentation();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la Representation : " . $laVue->getUneRepresentation()->getId()
                    . " (" . $laVue->getUneRepresentation()->getId() . ")");
            $this->vue->setTitre("Festival - Representtion");
            parent::controlerVueAutorisee();
            $this->vue->afficher();
        }
    }

//    /** controleur= groupe & action=creer
//     * Afficher le formulaire de création d'un groupe     */
//    public function creer() {
//        $laVue = new VueSaisieGroupe();
//        $this->vue = $laVue;
//        // Lire dans la BDD les données du groupe à modifier
//        Bdd::connecter();
//        $laVue->setUnGroupe(new Groupe("", "","" ,"", "", "", ""));
//        $laVue->setActionRecue("creer");
//        $laVue->setActionAEnvoyer("validerCreer");
//        $laVue->setMessage("Créer un nouveau Groupe");
//        parent::controlerVueAutorisee();
//        $this->vue->setTitre("Festival - Groupes");
//        $this->vue->afficher();
//    }

//    /** controleur= groupe & action=validerCreer
//     * modifier un groupe dans la base de données d'après la saisie    */
//    public function validerCreer() {
//        Bdd::connecter();
//        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
//       $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identite'], $_REQUEST['adresse'], $_REQUEST['nombre'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
//
//        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
//        // pour un formulaire de modification (paramètre n°1 = false)
//        $this->verifierDonneesGroupe($unGroupe, true);
//        if (GestionErreurs::nbErreurs() == 0) {
//            // s'il ny a pas d'erreurs,
//            // enregistrer les modifications pour l'établissement
//            GroupeDAO::insert($unGroupe);
//            // revenir à la liste des Groupes
//            header("Location: index.php?controleur=groupes&action=liste");
//        } else {
//            // s'il y a des erreurs, 
//            // réafficher le formulaire de saisie
//            $laVue = new VueSaisieGroupe();
//            $this->vue = $laVue;
//            $laVue->setUnGroupe($unGroupe);
//            $laVue->setActionRecue("creer");
//            $laVue->setActionAEnvoyer("validerCreer");
//            $laVue->setMessage("Créer un nouveau Groupe");
//            $this->vue->setTitre("Festival - Groupes");
//            parent::controlerVueAutorisee();
//            $this->vue->afficher();
//        }
//    }

    /** controleur= representation & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRepres = $_GET["id"];
        $laVue = new VueSupprimerRepresentation();
        $this->vue = $laVue;
        // Lire dans la BDD la representation à supprimer
        Bdd::connecter();
        $laVue->setUneRepresentation(RepresentationDAO::getOneById($idRepres));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }
    
    /** controleur= representation & action= validerSupprimer & id = n° representation
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de la representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representation
        header("Location: index.php?controleur=representation&action=consulter");
    }

 /**
     * Vérification de la saisie des données du formulaire
     * @param Groupe $unGroupe
     * @param bool $creation
     */
    private function verifierDonneesRepresentation(Representation $uneRepresentation, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if ($creation && $uneRepresentation->getId() == "" || $uneRepresentation->getDate() == ""|| $uneRepresentation->getGroupe() == ""|| $uneRepresentation->getLieu() == ""|| $uneRepresentation->getHeureDebut() == ""|| $uneRepresentation->getHeureFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepresentation->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estNumerique($uneRepresentation->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRepresentation->getId())) {
                    GestionErreurs::ajouter("La Representation " . $uneRepresentation->getId() . " existe déjà");
                }
            }
        }
       }

}
