<?php
namespace modele\dao;

use modele\metier\Lieu;
use PDO;

/**
 * Description of LieuDAO
 * Classe métier  :  Lieu
 * @author Lucas LELONG
 * @version 2017
 */
class LieuDAO {
    /**
     * crée un objet métier à partir d'un enregistrement
     * @param array $enreg
     * @return Lieu objet métier obtenu
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $nom = $enreg['NOM'];
        $adresse = $enreg['ADRESSE'];
        $capacite = $enreg['CAPACITE'];
        
        $objetMetier = new Lieu($id, $nom, $adresse, $capacite);
        return $objetMetier;
    }
  /**
     * Recherche un lieu selon la valeur de son id
     * @param string $id valeur de l'id recherchée
     * @return Lieu  Le lieu trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    public static function getOneByNom($nom) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE NOM = :nom";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':nom', $nom);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Retourne la liste de tous les Lieux
     * @return array tableau d'objets de type Lieu
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier un Lieu et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
}
