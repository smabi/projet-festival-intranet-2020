<?php

namespace modele\dao;

use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use \PDOStatement;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier : Representation
 */
class RepresentationDAO {

    /**
     * crée un objet métier à partir d'un enregistrement 
     * @param array $enreg
     * @return objet métier Representation
     */
     protected static function enregVersMetier(array $enreg) {
        $id = $enreg['IDREPRESENTATION'];
        $date = $enreg['DATEREPRES'];
        $lieu = $enreg['IDLIEU'];
        $lieu = LieuDAO::getOneById($lieu);
        $groupe = $enreg['IDGROUPE'];
        $groupe = GroupeDAO::getOneById($groupe);
        $heuredebut = $enreg['HEUREDEBUT'];
        $heurefin = $enreg['HEUREFIN'];
     
        

        $uneRepresentation = new Representation($id, $date, $lieu, $groupe, $heuredebut, $heurefin);

        return $uneRepresentation;
    }
    protected static function enregVersDate(array $enreg) {
            $date[] = $enreg['DATEREPRES'];
            return $date;
        }
    

    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param type $objetMetier une Representation
     * @param type $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':date', $objetMetier->getDate());
        $stmt->bindValue(':idlieu', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':idgroupe', $objetMetier->getGroupe()->getId());
        $stmt->bindValue(':heuredebut', $objetMetier->getHeuredebut());
        $stmt->bindValue(':heurefin', $objetMetier->getHeurefin());
    }
    
    /**
     * Retourne la liste de toutes les Representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier une Representation et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvée ; null sinon
     */
     public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE IDREPRESENTATION = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
public static function getAllDate(){
    $lesObjets = array();
        $requete = "SELECT DISTINCT dateRepres FROM Representation ";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok) {
             // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier une Representation et l'ajouter au tableau
                $lesObjets[] = self::enregVersDate($enreg);
            }
        }
        return $lesObjets;
    }


    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean = FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id, :date, :lieu, :groupe, :heuredebut, :heurefin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean = FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE  Representation SET  DATEREPRES=:date,
           IDLIEU=:idlieu, IDGROUPE=:idgroupe, HEUREDEBUT=:heuredebut,
           HEUREFIN=:heurefin
           WHERE IDREPRESENTATION=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);  
        RepresentationDAO::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

     /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean = TRUE si l'enregistrement est détruit, = FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM REPRESENTATION WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
}

