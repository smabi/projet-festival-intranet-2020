<?php
namespace modele\metier;

/**
 * Description of Lieu
 * un lieu musical se produisant au festival
 * @author prof
 */
class Lieu {
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $nom;
    /**
     * @var string 
     */
    private $adresse;
    /**
     * @var string
     */
    private $capacite;

    function __construct($id, $nom, $adresse, $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }

    function getId() {
        return $this->id;
    }

    function getNom() {
        return $this->nom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getCapacite() {
        return $this->capacite;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite($capacite) {
        $this->capacite = $capacite;
    }

}
