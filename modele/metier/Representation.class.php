<?php
namespace modele\metier;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Representation {
    
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $date;
    /**
     * @var modele\metier\Lieu
     */
    private $lieu;
    /**
     * @var modele\metier\Groupe
     */
    private $groupe;
    /**
     * @var string
     */
    private $heuredebut;
    /**
     * @var string
     */
    private $heurefin;
    
    function __construct(string $id, string $date, Lieu $lieu, Groupe $groupe, String $heuredebut, string $heurefin) {
        $this->id = $id;
        $this->date = $date;
        $this->lieu = $lieu;
        $this->groupe = $groupe;
        $this->heuredebut = $heuredebut;
        $this->heurefin = $heurefin;
    }
    
    function getId(): string {
        return $this->id;
    }

    function getDate(): string {
        return $this->date;
    }


    function getHeuredebut(): string {
        return $this->heuredebut;
    }

    function getHeurefin(): string {
        return $this->heurefin;
    }

    function setId(string $id) {
        $this->id = $id;
    }

    function setDate(string $date) {
        $this->date = $date;
    }


    function setHeuredebut(string $heuredebut) {
        $this->heuredebut = $heuredebut;
    }

    function setHeurefin(string $heurefin) {
        $this->heurefin = $heurefin;
    }

    function getLieu():Lieu {
        return $this->lieu;
    }

    function getGroupe(): Groupe {
        return $this->groupe;
    }

    function setLieu(Lieu $lieu) {
        $this->lieu = $lieu;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }




}
