<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Lieu : test</title>
    </head>

    <body>

    <?php

        use modele\dao\LieuDAO;
        use modele\dao\Bdd;
        use controleur\Session;

        require_once __DIR__ . '/../../includes/autoload.inc.php';
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test de LieuDAO</h2>";
              
        
        echo "<h3>1- Test getOneById</h3>";
        $id='001';
        try {
            $objetConstruit = LieuDAO::getOneById($id);
            var_dump($objetConstruit);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
       // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = LieuDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        
         Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>

