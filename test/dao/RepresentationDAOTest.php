<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

        <?php

        use modele\dao\RepresentationDAO;
        use modele\dao\Bdd;
        use modele\metier\Representation;
        use controleur\Session;

        require_once __DIR__ . '/../../includes/autoload.inc.php';

        Session::demarrer();
        Bdd::connecter();

        echo "<h2>1- RepresentationDAO</h2>";

        $id="001";
        // Test n°1
        echo "<h3>2- getAll</h3>";
        try {
            $lesObjets = RepresentationDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
               // Test n°2
        echo "<h3>Test getOneById</h3>";
        $id='001';
        try {
            $objet = RepresentationDAO::getOneById($id);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        
        // Test n°3
        //echo "<h3>3- insert</h3>";
        //try {
          //  $id = '9999999A';
            //$objet = new Representation($id, '2021-06-12', 'Le Parc', 'Groupe folklorique du Bachkortostan', '20h00','00h00');
            //$ok = RepresentationDAO::insert($objet);
            //if ($ok) {
              //  echo "<h4>ooo réussite de l'insertion ooo</h4>";
               // $objetLu = RepresentationDAO::getOneById($id);
              //  var_dump($objetLu);
            //} else {
              //  echo "<h4>*** échec de l'insertion ***</h4>";
           // }
       // } catch (Exception $e) {
         //   echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        //}


        // Test n°3
       // echo "<h3>4- update</h3>";
        //try {
         //   $objet->setHeuredebut('21h30');
           // $objet->setHeureFin('23h00');
           // $ok = RepresentationDAO::update($id, $objet);
            //if ($ok) {
             //   echo "<h4>ooo réussite de la mise à jour ooo</h4>";
               // $objetLu = RepresentationDAO::getOneById($id);
                //var_dump($objetLu);
            //} else {
              //  echo "<h4>*** échec de la mise à jour ***</h4>";
            //}
       // } catch (Exception $e) {
         //   echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
       // }

        // Test n°4
        //echo "<h3>5- delete</h3>";
       // try {
         //   $ok = RepresentationDAO::delete($id);
//         //   $ok = RepresentationDAO::delete("xxx");
           // if ($ok) {
             //   echo "<h4>ooo réussite de la suppression ooo</h4>";
           // } else {
            //    echo "<h4>*** échec de la suppression ***</h4>";
           // }
        //} catch (Exception $e) {
          //  echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
       // }
        ?>


    </body>
</html>
