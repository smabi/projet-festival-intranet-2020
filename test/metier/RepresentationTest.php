<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
          use modele\metier\Lieu;
        use modele\metier\Groupe;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        $lieu= new Lieu(003,"La Bastille", "33 rue du test", 12000);
        $groupe = new Groupe("g502", "THE ROCK", null, null, 15, "Espagne", "N" );
        $objet = new Representation(002,"10/11/2021",$lieu,$groupe,"12h00","12h50");
        var_dump($objet);
        ?>
    </body>
</html>