<?php

namespace vue\groupe;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de consultation d'un Groupe donné

 */
class VueDetailGroupe extends VueGenerique {

    /** @var Groupe identificateur du Groupe à afficher */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br>
        <table width='20%' cellspacing='0' cellpadding='0'  class='tabNonQuadrille'> 
            <tr class='enTeteTabNonQuad'>
                <td colspan='4'> <strong>Groupe</strong> </td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Id :</strong></td>
                <td colspan='3'><strong><?= $this->unGroupe->getId() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nom :</strong></td>
                <td colspan='3'><strong><?= $this->unGroupe->getNom() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Identité responsable :</strong></td>
                <td colspan='3'><strong><?= $this->unGroupe->getIdentite() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Adresse postale :</strong></td>
                <td colspan='3'><strong><?= $this->unGroupe->getAdresse() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nombres de personnes:</strong>
                <td colspan='3'><strong><?= $this->unGroupe->getNbPers() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Nom pays :</strong>
                <td colspan='3'><strong><?= $this->unGroupe->getNomPays() ?></strong></td>
            </tr>
            <tr class="ligneTabNonQuad">
                <td colspan='3'><strong>Hebergement (O/N) :</strong></td>
                <td colspan='3'><strong><?= $this->unGroupe->getHebergement() ?></strong></td>
            </tr>
        </table>
        <br>
        <a href='index.php?controleur=groupes&action=liste'>Retour</a>
        <?php
        include $this->getPied();
    }

    function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }

}
