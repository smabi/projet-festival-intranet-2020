<?php

namespace vue\groupe;

use vue\VueGenerique;

class VueListeGroupe extends VueGenerique {

    /** @var array liste des Groupes à afficher  */
    private $lesGroupes;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Implémentation de la méthode générant le code HTML de la page concernée
     */
    public function afficher() {
        include $this->getEntete();
        ?>
        
        <br>
        <table width='40%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'>
            <tr class='enTeteTabNonQuad'>
                <td colspan='5'><strong>Groupe</strong></td>
            </tr>
            <?php
            // Pour chaque Groupe
            foreach ($this->lesGroupes as $unGroupe) {
                ?>
                <tr class='ligneTabNonQuad'> 
                    <td width='15%'><?= $unGroupe->getId() ?></td>
                    <td width='33%'><?= $unGroupe->getNom() ?></td>
                    <td width='15%' align='center'>         
                        <a href="index.php?controleur=groupes&action=modifier&id=<?= $unGroupe->getId() ?>">
                            Modifier       
                        </a></td>
                        <td width='15%' align='center'> 
                        <a href="index.php?controleur=groupes&action=supprimer&id=<?= $unGroupe->getId() ?>">
                            Supprimer               
                        </a></td>
                            <td width='15%' align='center'> 
                         <a href="index.php?controleur=groupes&action=detail&id=<?= $unGroupe->getId() ?>">
                            Voir Details
                        </a></td>
                    <?php
                   
            }
            ?>    
        </table><br>
        <a href='index.php?controleur=groupes&action=creer'>
            Création d'un Groupe
        </a>      
        <?php
        include $this->getPied();
    }

    // ACCESSEUR et MUTATEURS
    public function setGroupe(Array $lesGroupes) {
        $this->lesGroupes = $lesGroupes;
    }


}