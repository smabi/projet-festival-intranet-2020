<?php

namespace vue\groupe;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de suppression d'un type de chambre donné
 * @author prof
 * @version 2018
 */
class VueSupprimerGroupe extends VueGenerique {

    /** @var Groupe groupe à modifier */
    private $unGroupe;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer le groupe 
        <?= $this->unGroupe->getId() ?> <?= $this->unGroupe->getNom() ?> ?
            <h3><br>
                <a href="index.php?controleur=groupes&action=validerSupprimer&id=<?= $this->unGroupe->getId() ?>">
                    Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="index.php?controleur=groupes">Non</a></h3></center>
        <?php
        include $this->getPied();
    }

    public function getUnGroupe(): Groupe {
        return $this->unGroupe;
    }

    public function getActionRecue() {
        return $this->actionRecue;
    }

    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }

    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

}