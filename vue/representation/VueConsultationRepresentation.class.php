<?php
namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;

class VueConsultationRepresentation extends VueGenerique {

    /** @var array liste des représentation */
    private $lesRepresentations;
    private $lesDates;
    
    

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        // IL FAUT QU'IL Y AIT AU MOINS UNE REPRESENTATION POUR QUE L'AFFICHAGE SOIT EFFECTUÉ        
        if (count($this->lesRepresentations) != 0) {
            
            $this->lesDates = RepresentationDAO::getAllDate();
            // POUR CHAQUE DATE : AFFICHAGE DE LA DATE ET D'UN TABLEAU COMPORTANT 1
            // LIGNE D'EN-TÊTE ET 1 LIGNE PAR REPRESENTATION
            if(is_array($this->lesDates)){
                
            foreach ($this->lesDates as $uneDate) {             
                ?>
                <strong><?= implode($uneDate) ?></strong><br>
                
                <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                    <tr class="enTeteTabQuad">
                        <td width="30%">Lieu</td>
                        <td width="35%">Groupe</td>
                        <td width="35%">Heure Début</td> 
                        <td width="35%">Heure Fin</td> 
                        <td width="15%"> </td>
                        <td width="15%"> </td>
                    </tr>
                    <?php
                    // BOUCLE SUR LES Representation (AFFICHAGE D'UNE LIGNE PAR REPRESENTATION)
                    /* @var Representation $uneRepresentation */
                    foreach ($this->lesRepresentations as $uneRepresentation) {
                        ?>
                        <tr class="ligneTabQuad">
                            <?php if ($uneRepresentation->getDate() == (implode($uneDate))){
                                
                            ?>
                            <td><?= $uneRepresentation->getLieu()->getNom() ?></td>
                            <td><?= $uneRepresentation->getGroupe()->getNom() ?></td>
                            <td><?= $uneRepresentation->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation->getHeureFin() ?></td>
                            <td width='15%' align='center'>         
                        <a href="index.php?controleur=representation&action=modifier&id=<?= $uneRepresentation->getId() ?>">
                            Modifier       
                        </a></td>
                        <td width='15%' align='center'> 
                        <a href="index.php?controleur=representation&action=supprimer&id=<?= $uneRepresentation->getId() ?>">
                            Supprimer               
                        </a></td>
                            <?php }   ?>
                        </tr>
                        <?php
                    }
                    
                    
                    ?>
                </table><br>
                <?php
            }
            include $this->getPied();
        }
        }
    }

    function getLesRepresentations(): array {
        return $this->lesRepresentations;
    }

    function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }


    }
    

