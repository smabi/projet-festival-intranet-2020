<?php

namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;

class VueSaisieRepresentation extends VueGenerique {

    /** @var Representation representation à modifier */
    private $uneRepresentation;

    private $lesLieux;
     private $lesGroupes;
    
    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representation&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>
                <?php
                // En cas de création, l'id est accessible sinon l'id est dans un champ
                // caché
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->getUneRepresentation()->getId() ?>" name="id" size ="2"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr class="autreLigne">
                        <td><input type="hidden" value="<?= $this->getUneRepresentation()->getId() ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>

                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="date" value="<?= $this->getUneRepresentation()->getDate() ?>" name="date" size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <td><SELECT name="lieu" size="1">
                            
                           <?php $lesLieux = LieuDAO::getAll();
                           foreach ($lesLieux as $unLieu) { ?>
                            <OPTION><?php echo $unLieu->getNom();?></OPTION>
                           <?php }?>
                            
                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Groupe*: </td>
                    <td><SELECT name="groupe" size="1">
                            
                           <?php $lesGroupes = GroupeDAO::getAll();
                           foreach ($lesGroupes as $unGroupe) { ?>
                            <OPTION><?php echo $unGroupe->getNom();?></OPTION>
                           <?php }?>
                            
                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de début*: </td>
                    <td><input type="hour" value="<?= $this->getUneRepresentation()->getHeuredebut() ?>" name="heuredebut" size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de Fin*: </td>
                    <td><input type="hour" value="<?= $this->getUneRepresentation()->getHeurefin() ?>" name="heurefin" size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">

                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler"> </td>
                </tr>
            </table>
            <a href="index.php?controleur=representation&action=consulter">Retour</a>
        </form>        
        <?php
        include $this->getPied();
    }

    function getUneRepresentation(): Representation {
        return $this->uneRepresentation;
    }

    function getActionRecue(): string {
        return $this->actionRecue;
    }

    function getActionAEnvoyer(): string {
        return $this->actionAEnvoyer;
    }

    function getMessage(): string {
        return $this->message;
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

    function setActionRecue(string $actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    function setActionAEnvoyer(string $actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    function setMessage(string $message) {
        $this->message = $message;
    }

}
