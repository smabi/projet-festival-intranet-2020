<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;

/**
 * Description Page de suppression d'une representations donnée
 * @author Lucas
 * @version 2020
 */
class VueSupprimerRepresentation extends VueGenerique {

    /** @var Representation representation à modifier */
    private $uneRepresentation;

    function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer la représentation donnée par le groupe :
        <?= $this->uneRepresentation->getLeGroupe()->getNom() ?> , au/à  <?= $this->uneRepresentation->getLeLieu()->getNom() ?> ?
            <h3><br>
                <a href="index.php?controleur=representation&action=validerSupprimer&id=<?= $this->uneRepresentation->getId() ?>">
                    Oui</a>
                <a href="index.php?controleur=representation">Non</a></h3></center>
        <?php
        include $this->getPied();
    }
    
   
    
    // ACCESSEURS ET MUTATEURS
    function getUneRepresentation(): Representation {
        return $this->uneRepresentation;
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

    public function getActionRecue() {
        return $this->actionRecue;
    }
    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }
    public function getMessage() {
        return $this->message;
    }
    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }
    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }
    public function setMessage($message) {
        $this->message = $message;
    }

}